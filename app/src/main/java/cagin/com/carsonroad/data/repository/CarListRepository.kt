package cagin.com.carsonroad.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.util.Log
import cagin.com.carsonroad.data.retrofit.implementation.CarListImplementation
import cagin.com.carsonroad.data.retrofit.model.CarModel
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class CarListRepository @Inject constructor(carListImp: CarListImplementation) {
    private val carListImplementation: CarListImplementation = carListImp
    private val TAG: String = "WebResponse"

    fun sendCarListRequest(): LiveData<List<CarModel>> {
        val liveData: MutableLiveData<List<CarModel>> = MutableLiveData()
        carListImplementation.sendCarRequest().enqueue((object : retrofit2.Callback<List<CarModel>> {
            override fun onFailure(call: Call<List<CarModel>>?, t: Throwable?) {
                Log.e(TAG, t.toString())
                liveData.value = null
            }
            override fun onResponse(call: Call<List<CarModel>>?, response: Response<List<CarModel>>?) {
                Log.i(TAG, response?.message())
                liveData.value = response?.body()
            }
        }))
        return liveData
    }
}