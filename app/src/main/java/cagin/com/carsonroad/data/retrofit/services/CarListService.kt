package cagin.com.carsonroad.data.retrofit.services

import cagin.com.carsonroad.data.retrofit.model.CarModel
import retrofit2.Call
import retrofit2.http.GET

interface CarListService {
    @GET("cars")
    fun sendCarRequest(): Call<List<CarModel>>
}