package cagin.com.carsonroad.data.retrofit.implementation

import cagin.com.carsonroad.data.retrofit.model.CarModel
import cagin.com.carsonroad.data.retrofit.services.CarListService
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

class CarListImplementation @Inject constructor(builder: Retrofit.Builder) : CarListService {
    private val carListService: CarListService = builder.build().create(CarListService::class.java)

    override fun sendCarRequest(): Call<List<CarModel>> {
        return carListService.sendCarRequest()
    }
}