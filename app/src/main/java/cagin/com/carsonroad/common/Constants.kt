package cagin.com.carsonroad.common

class Constants {
    object Api {
        val BASE_URL = "https://cdn.sixt.io/codingtask/"
    }
    object Data{
        const val LIST_KEY = "CAR_LIST"
        const val CAR_ITEM_KEY = "CAR"
    }
    object Fragment{
        const val BACKSTACK_KEY = "CARLISTFRAGMENT"
    }
    object Dagger{
        const val FRAGMENT_NAME = "CARLISTFRAGMENT"
    }
}