package cagin.com.carsonroad.feature.carlistfragment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import javax.inject.Inject

class CarListViewModel @Inject constructor(application: Application) : AndroidViewModel(application)