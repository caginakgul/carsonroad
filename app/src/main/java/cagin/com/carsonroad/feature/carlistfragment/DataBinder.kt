package cagin.com.carsonroad.feature.carlistfragment

import androidx.databinding.BindingAdapter
import android.widget.ImageView
import cagin.com.carsonroad.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("android:carImage")
fun setImageUrl(imageView: ImageView, url: String?) {
    Glide.with(imageView.context)
        .load(url)
        .apply(
            RequestOptions()
                .placeholder(R.drawable.car_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        )
        .into(imageView)
}
