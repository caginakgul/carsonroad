package cagin.com.carsonroad.feature.carlistfragment

import androidx.lifecycle.ViewModelProvider
import cagin.com.carsonroad.base.ProjectViewModelFactory
import cagin.com.carsonroad.common.Constants
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class CarListFragmentModule {
    @Provides
    @Named(Constants.Dagger.FRAGMENT_NAME)
    fun provideCarListViewModel(carListViewModel: CarListViewModel): ViewModelProvider.Factory{
        return ProjectViewModelFactory(carListViewModel)
    }
}