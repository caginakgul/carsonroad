package cagin.com.carsonroad.feature.bottomsheet

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cagin.com.carsonroad.R
import cagin.com.carsonroad.common.Constants
import cagin.com.carsonroad.data.retrofit.model.CarModel
import cagin.com.carsonroad.databinding.FragmentBottomSheetBinding

class BottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var bottomSheetListener: BottomSheetListener
    private lateinit var binding: FragmentBottomSheetBinding

    companion object {
        fun newInstance(carModel: CarModel?): BottomSheetFragment {
            val args = Bundle()
            args.putParcelable(Constants.Data.CAR_ITEM_KEY, carModel)
            val fragment = BottomSheetFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_bottom_sheet, container, false
        )
        val args = arguments
        val carItem = args?.getParcelable<CarModel>(Constants.Data.CAR_ITEM_KEY)
        binding.carItem = carItem
        binding.executePendingBindings()
        return binding.root
    }

    interface BottomSheetListener {
        fun onOptionClick(text: String)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            bottomSheetListener = context as BottomSheetListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context?.toString())
        }
    }
}