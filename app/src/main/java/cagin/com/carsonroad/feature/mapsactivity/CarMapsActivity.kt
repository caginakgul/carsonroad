package cagin.com.carsonroad.feature.mapsactivity

import android.Manifest
import android.annotation.SuppressLint
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.View.*
import android.widget.Toast
import androidx.fragment.app.Fragment

import cagin.com.carsonroad.R
import cagin.com.carsonroad.common.ActivityUtil
import cagin.com.carsonroad.common.Constants
import cagin.com.carsonroad.data.retrofit.model.CarModel
import cagin.com.carsonroad.databinding.ActivityCarMapsBinding
import cagin.com.carsonroad.feature.bottomsheet.BottomSheetFragment
import cagin.com.carsonroad.feature.carlistfragment.CarListFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class CarMapsActivity : FragmentActivity(), OnMapReadyCallback,
    BottomSheetFragment.BottomSheetListener, HasSupportFragmentInjector {

    private lateinit var binding: ActivityCarMapsBinding
    private lateinit var viewModel: CarMapsViewModel
    private var mMap: GoogleMap? = null
    private lateinit var carList: ArrayList<CarModel>
    private val BOTTOMSHEET_TAG: String = "BottomSheet"
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        Timber.i("onCreated")
        super.onCreate(savedInstanceState)
        initBinding()
        sendCarListRequest()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return supportFragmentInjector
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        userLocation()
    }

    override fun onOptionClick(text: String) {
       Timber.i(text)
    }

    private fun initBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_car_maps)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CarMapsViewModel::class.java)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        binding.imageViewList.setOnClickListener { startListFragment() }
        binding.imageViewList.visibility = GONE
    }

    private fun sendCarListRequest() {
        viewModel.sendCarListRequest().observe(this,
            Observer<List<CarModel>> { networkResponse ->
                hideProgress()
                if (networkResponse != null) {
                    Timber.i("data size: %s", networkResponse.size)
                    setMarkers(networkResponse)
                    binding.imageViewList.visibility = VISIBLE
                    carList = networkResponse as ArrayList<CarModel>
                } else {
                    Timber.w("connection failed.")
                    ActivityUtil.displaySnack("Please check your internet connection!",
                        binding.constraintLayoutRootMaps)
                }
            })
    }

    private fun setMarkers(carList: List<CarModel>?) {
        for (car: CarModel in carList!!) {
            val carLocation = LatLng(car.latitude, car.longitude)
            val marker = mMap?.addMarker(
                MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.car_marker))
                    .position(carLocation).title(car.name)
            )
            marker?.tag = car.id
        }
        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(carList[0].latitude, carList[0].longitude), 12f))
        mMap?.setOnMarkerClickListener { marker ->
            BottomSheetFragment.newInstance(carList.find { it.id == marker.tag })
                .show(supportFragmentManager, BOTTOMSHEET_TAG)
            true
        }
    }

    private fun startListFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(
                R.id.constraintLayoutRootMaps,
                CarListFragment.newInstance(carList),
                Constants.Fragment.BACKSTACK_KEY
            )
            .addToBackStack(Constants.Data.LIST_KEY)
            .commit()
    }

    private fun userLocation() {
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
            Timber.i("Permission has been granted by user")
        } else {
            mMap?.isMyLocationEnabled = true
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 101
        )
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            101 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Timber.w("Permission has been denied by user")
                } else {
                    Timber.i("Permission has been granted by user")
                    mMap?.isMyLocationEnabled = true
                }
            }
        }
    }

    private fun hideProgress() {
        binding.progressBar.visibility = GONE
    }

}
