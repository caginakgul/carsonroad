package cagin.com.carsonroad.feature.carlistfragment

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import cagin.com.carsonroad.data.retrofit.model.CarModel
import cagin.com.carsonroad.databinding.ItemCarListBinding

class CarListAdapter(private val items: List<CarModel>?) : RecyclerView.Adapter<CarListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCarListBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items!![position])

    inner class ViewHolder(private val binding: ItemCarListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CarModel) {
            binding.carItem = item
            binding.executePendingBindings()
        }
    }
}