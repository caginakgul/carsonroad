package cagin.com.carsonroad.feature.carlistfragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CarListFragmentProvider {
    @ContributesAndroidInjector(modules = [(CarListFragmentModule::class)])
    abstract fun contributeCarListFragment(): CarListFragment
}