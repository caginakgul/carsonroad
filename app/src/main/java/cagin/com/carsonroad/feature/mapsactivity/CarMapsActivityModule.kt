package cagin.com.carsonroad.feature.mapsactivity

import androidx.lifecycle.ViewModelProvider
import cagin.com.carsonroad.base.ProjectViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class CarMapsActivityModule {
    @Provides
    fun provideCarListViewModel(carListViewModel: CarMapsViewModel): ViewModelProvider.Factory {
        return ProjectViewModelFactory(carListViewModel)
    }
}