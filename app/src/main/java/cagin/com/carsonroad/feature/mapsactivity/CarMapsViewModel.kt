package cagin.com.carsonroad.feature.mapsactivity

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cagin.com.carsonroad.common.ConnectionUtil
import cagin.com.carsonroad.data.repository.CarListRepository
import cagin.com.carsonroad.data.retrofit.model.CarModel
import javax.inject.Inject

class CarMapsViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {
    @Inject
    lateinit var carListRepo: CarListRepository

    fun sendCarListRequest(): LiveData<List<CarModel>> {
        if (ConnectionUtil.isConnectedOrConnecting(getApplication())) {
            return carListRepo.sendCarListRequest()
        } else {
            val liveData: MutableLiveData<List<CarModel>> = MutableLiveData()
            liveData.value = null
            return liveData
        }
    }
}