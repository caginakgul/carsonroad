package cagin.com.carsonroad.feature.carlistfragment

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cagin.com.carsonroad.R
import cagin.com.carsonroad.common.Constants
import cagin.com.carsonroad.data.retrofit.model.CarModel
import cagin.com.carsonroad.databinding.FragmentCarListBinding
import dagger.android.support.AndroidSupportInjection

class CarListFragment : Fragment() {
    private lateinit var binding: FragmentCarListBinding

    companion object {
        fun newInstance(carList: ArrayList<CarModel>?): CarListFragment {
            val args = Bundle()
            args.putParcelableArrayList(Constants.Data.LIST_KEY, carList as ArrayList<out Parcelable>?)
            val fragment = CarListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_car_list, container, false
        )
        val args = arguments
        val carList = args?.getParcelableArrayList<CarModel>(Constants.Data.LIST_KEY)
        setRecycler(carList)
        return binding.root
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun setRecycler(carList: ArrayList<CarModel>?) {
        binding.recyclerViewCarList.layoutManager = LinearLayoutManager(activity)
        binding.recyclerViewCarList.adapter = CarListAdapter(carList)
    }
}