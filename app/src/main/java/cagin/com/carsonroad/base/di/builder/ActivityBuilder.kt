package cagin.com.carsonroad.base.di.builder

import cagin.com.carsonroad.feature.carlistfragment.CarListFragmentModule
import cagin.com.carsonroad.feature.carlistfragment.CarListFragmentProvider
import cagin.com.carsonroad.feature.mapsactivity.CarMapsActivity
import cagin.com.carsonroad.feature.mapsactivity.CarMapsActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(
        modules = [CarMapsActivityModule::class, CarListFragmentProvider::class]
    )
    internal abstract fun contributeCarMapsActivity(): CarMapsActivity
}