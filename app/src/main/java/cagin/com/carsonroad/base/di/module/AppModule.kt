package cagin.com.carsonroad.base.di.module

import cagin.com.carsonroad.CarsOnRoadApplication
import dagger.Module

@Module
class AppModule(val app: CarsOnRoadApplication)