package cagin.com.carsonroad.base.di.components

import android.app.Application
import cagin.com.carsonroad.CarsOnRoadApplication
import cagin.com.carsonroad.base.di.builder.ActivityBuilder
import cagin.com.carsonroad.base.di.module.AppModule
import cagin.com.carsonroad.data.di.RetrofitModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        RetrofitModule::class,
        AndroidSupportInjectionModule::class,
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityBuilder::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: CarsOnRoadApplication)
}