package cagin.com.carsonroad

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import cagin.com.carsonroad.feature.mapsactivity.CarMapsActivity
import org.junit.Rule
import androidx.test.rule.ActivityTestRule
import org.junit.Test

class CarMapsActivityTest {
    @Rule
    @JvmField
    val activity = ActivityTestRule<CarMapsActivity>(CarMapsActivity::class.java)

    /*
        should not be clickable until the data is retrieved
     */
    @Test
    fun listViewClickable() {
        onView(withId(R.id.imageViewList)).perform(click())
    }

    @Test
    fun progressBarVisible() {
        onView(withId(R.id.progressBar))
            .check(matches(isDisplayed()))
    }

    @Test
    fun mapVisible() {
        onView(withId(R.id.map))
            .check(matches(isDisplayed()))
    }
}